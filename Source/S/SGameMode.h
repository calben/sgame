// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.
#pragma once
#include "GameFramework/GameMode.h"
#include "SGameMode.generated.h"

UCLASS(minimalapi)
class ASGameMode : public AGameMode
{
	GENERATED_BODY()

public:
	ASGameMode();

	void CheckMatchEnd();

	void FinishMatch();

	void RespawnPlayer(class AController* NewPlayer);

};



